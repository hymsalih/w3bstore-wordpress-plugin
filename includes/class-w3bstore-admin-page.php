<?php
define('W3B_ADMIN_TEMPLATES_DIR', W3B_PLUGIN_DIR . 'template\admin');

class W3bStore_Admin_Page
{
    public static function w3bs_connect_page()
    {
        require_once W3B_ADMIN_TEMPLATES_DIR . '\welcome-page.php';
    }

    public static function w3bs_storefront_page()
    {
        require_once W3B_ADMIN_TEMPLATES_DIR . '\storefront-page.php';
    }

    public static function w3bs_help_page()
    {
        require_once W3B_ADMIN_TEMPLATES_DIR . '\help-page.php';
    }
}
