<?php

class W3bStore_Admin
{
	const ADMIN_SLUG = 'w3b-store';
    public function __construct()
    {
        if (is_admin()) {
            add_action('admin_menu', array($this, 'build_menu'));
        }
    }
    public function build_menu()
    {
        $page = new W3bStore_Admin_Page();
		
		add_menu_page(
			__( 'Setup', 'w3bstore' ),
			'W3bStore',
			'manage_options',
			self::ADMIN_SLUG,
			array( $page, 'w3bs_connect_page' ),
			'dashicons-store',
			'2.562347345'
        );
        
        add_submenu_page(
            self::ADMIN_SLUG,
            __( 'Setup', 'w3bstore' ),
            __( 'Setup', 'w3bstore' ),
            'manage_options',
            self::ADMIN_SLUG,
            array( $page, 'w3bs_connect_page' ),
        );

        if(self::is_auth()){
            add_submenu_page(
                self::ADMIN_SLUG,
                __( 'Menu 1', 'w3bstore' ),
                __( 'Menu 1', 'w3bstore' ),
                'manage_options',
                'w3bs-menu-1',
                // array( $page, 'w3bs_help_page' ),
            );
            add_submenu_page(
                self::ADMIN_SLUG,
                __( 'Menu 2', 'w3bstore' ),
                __( 'Menu 2', 'w3bstore' ),
                'manage_options',
                'w3bs-menu-2',
                // array( $page, 'w3bs_help_page' ),
            );
            add_submenu_page(
                self::ADMIN_SLUG,
                __( 'Menu 3', 'w3bstore' ),
                __( 'Menu 3', 'w3bstore' ),
                'manage_options',
                'w3bs-menu-3',
                // array( $page, 'w3bs_help_page' ),
            );
            add_submenu_page(
                self::ADMIN_SLUG,
                __( 'Menu 4', 'w3bstore' ),
                __( 'Menu 4', 'w3bstore' ),
                'manage_options',
                'w3bs-menu-4',
                // array( $page, 'w3bs_help_page' ),
            );
            add_submenu_page(
                self::ADMIN_SLUG,
                __( 'Menu 5', 'w3bstore' ),
                __( 'Menu 5', 'w3bstore' ),
                'manage_options',
                'w3bs-menu-5',
                // array( $page, 'w3bs_help_page' ),
            );
        }
        
        add_submenu_page(
            self::ADMIN_SLUG,
            __( 'Storefront', 'w3bstore' ),
            __( 'Storefront', 'w3bstore' ),
            'manage_options',
            'w3bs-storefront',
            array( $page, 'w3bs_storefront_page' ),
        );
        
        
        add_submenu_page(
            self::ADMIN_SLUG,
            __( 'Help', 'w3bstore' ),
            __( 'Help', 'w3bstore' ),
            'manage_options',
            'w3bs-help',
            array( $page, 'w3bs_help_page' ),
        );
    }
	
	static public function get_relative_dashboard_url() {
		return 'admin.php?page=' . self::ADMIN_SLUG;
    }
    
    public static function is_auth(){
        global $wpdb, $table_prefix;
        $auth = false;
        $tblname = 'w3bstore_auth';
        $wp_w3bs_table = $table_prefix . "$tblname";
        $rows = $wpdb->get_results("SELECT COUNT(*) as num_rows FROM " . $wp_w3bs_table . ' where w3bstore_token <> ""');
        if ($rows[0]->num_rows && $rows[0]->num_rows != 0) {
            $auth = true;
        }
        return $auth;
    }
}
