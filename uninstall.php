<?php
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
$tblname = 'w3bstore_auth';
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}$tblname");