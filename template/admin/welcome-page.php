<div class="w3bstore w3bstore--welcome w3bconnect-page">
    <div class="w3bstore__body">
        <div class="w3b-content">
            <div class="w3b-logo">
                <img src="<?php echo W3B_PLUGIN_URL . '/images/w3bstore_logo.png' ?>">
                <!-- <?php echo file_get_contents(W3B_PLUGIN_URL . '/images/w3b-logo.svg'); ?> -->
            </div>
            <h2>
                <?php _e('Add an Online Store to Your Website', 'w3bstore'); ?>
            </h2>

            <?php
            // if ($state == 'create' || $state == 'connect') { 
            ?>

            <div class="w3b-subheading">
                <p>
                    <?php _e('Create a new store or connect an existing one, if you already have an W3bStore account. The plugin will guide you through store setup and help publish it on your website.', 'w3bstore'); ?>
                </p>
            </div>

            <?php
            // } 
            ?>

            <?php /*
             if ($state == 'no_oauth') { ?>

                <div class="w3b-subheading">
                    <p>
                        <?php echo sprintf(
                            __('To add your store to your website, put your %1$s Store ID in the field below. If you don\'t have an %1$s account yet, create one for free on the <a %2$s>%1$s website</a>.', 'w3bstore'),
                            Ecwid_Config::get_brand(),
                            'href="' . esc_attr(ecwid_get_register_link()) . '" target="_blank"'
                        ); ?>
                    </p>
                </div>

            <?php } 
            */
            ?>

            <div class="w3b-form">
                <div class="w3b-button">
                    <form action="<?php echo 'admin-post.php?action=w3bstore_connect'; ?>" method="post">
                        <button type="submit" class="btn btn--large btn--orange"><?php _e('Connect Your Store', 'w3bstore'); ?></button>
                    </form>
                </div>
                <a target="_blank" href="http://w3bstore.com" ><?php _e( 'Create store', 'w3bstore' ); ?>&nbsp;&rsaquo;</a>
            </div>

            <div class="w3b-note ">
                <?php
                _e('To display your store on this site, you need to allow WordPress to access your W3bStore products. Please press connect to provide permission.', 'w3bstore');
                // if( $ecwid_oauth->get_reconnect_message() ) {
                // 	echo $this->get_welcome_page_note( $ecwid_oauth->get_reconnect_message() );
                // }
                ?>
            </div>
        </div>
    </div>
</div>