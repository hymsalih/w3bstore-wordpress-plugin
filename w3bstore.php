<?php
/*
Plugin Name: W3bStore
Description: Connect your stores with W3bStore wordpress plugin.
Version: 1.0.0
Text Domain: w3bstore
*/

register_activation_hook(__FILE__, 'w3b_store_activate');
// register_deactivation_hook( __FILE__, 'w3b_store_deactivate' );

if (is_admin()) {
    add_action('admin_enqueue_scripts', 'w3bstore_register_admin_styles');
    add_action('admin_enqueue_scripts', 'w3bstore_register_admin_scripts');
    add_action('admin_post_w3bstore_connect', 'w3bs_admin_post_connect');
    add_action('admin_post_auth_w3bstore', 'w3bs_auth_w3bstore');
}

if (!defined('W3B_PLUGIN_DIR')) {
    define('W3B_PLUGIN_DIR', plugin_dir_path(__FILE__));
}

if (!defined('W3B_TEMPLATES_DIR')) {
    define('W3B_TEMPLATES_DIR', W3B_PLUGIN_DIR . 'templates');
}

if (!defined('W3B_PLUGIN_BASENAME')) {
    define('W3B_PLUGIN_BASENAME', plugin_basename(__FILE__));
}

if (!defined('W3B_PLUGIN_URL')) {
    define('W3B_PLUGIN_URL', plugin_dir_url(__FILE__));
}

if (!defined('W3B_SHORTCODES_DIR')) {
    define('W3B_SHORTCODES_DIR', W3B_PLUGIN_DIR . 'includes/shortcodes');
}

// if (!defined('W3B_AUTH_URL')) {
//     define('W3B_AUTH_URL', 'https://w3bstore.com/market.php/security');
// }

if (!defined('W3B_AUTH_URL')) {
    define('W3B_AUTH_URL', W3B_PLUGIN_URL . 'test_auth.php');
}

require_once W3B_PLUGIN_DIR . 'includes/class-w3bstore-admin.php';
require_once W3B_PLUGIN_DIR . 'includes/class-w3bstore-admin-page.php';

new W3bStore_Admin;

function w3b_store_activate()
{
    create_db_table();
}

function create_db_table()
{
    global $table_prefix, $wpdb;

    $tblname = 'w3bstore_auth';
    $wp_w3bs_table = $table_prefix . "$tblname";
    if ($wpdb->get_var("show tables like '$wp_w3bs_table'") != $wp_w3bs_table) {

        $sql = "CREATE TABLE `" . $wp_w3bs_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `token`  varchar(128)   NOT NULL, ";
        $sql .= "  `w3bstore_token`  varchar(128)   NOT NULL, ";
        $sql .= "  `auth_at`  varchar(50), ";
        $sql .= "  `created_at`  varchar(50) NOT NULL, ";
        $sql .= "  `token_created`  tinyint(1) NOT NULL DEFAULT 1, ";
        $sql .= "  primary key (id) ";
        $sql .= ")";
        require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

add_action('activated_plugin', 'w3bs_plugin_activation_redirect');
function w3bs_plugin_activation_redirect($plugin)
{
    if ($plugin == plugin_basename(__FILE__)) {
        exit(wp_safe_redirect(W3bStore_Admin::get_relative_dashboard_url()));
    }
}

function w3bstore_register_admin_styles($hook_suffix)
{
    wp_enqueue_style('w3b-admin-css', W3B_PLUGIN_URL . 'css/admin.css', array());
}

function w3bstore_register_admin_scripts($hook_suffix)
{
    wp_enqueue_script('w3b-admin-js', W3B_PLUGIN_URL . 'js/admin.js', array('jquery'));
    wp_localize_script('w3b-admin-js', 'w3bsJs', ['ajaxurl' => admin_url('admin-ajax.php')]);
}

function w3bs_admin_post_connect()
{
    global $wpdb, $table_prefix;
    if (!current_user_can('manage_options')) {
        return;
    }

    $token = bin2hex(random_bytes(20));
    $tblname = 'w3bstore_auth';
    $wp_w3bs_table = $table_prefix . "$tblname";

    $objDateTime = new DateTime('NOW');
    $dateTime =  $objDateTime->format('Y-m-d H:i:s');

    $rows = $wpdb->get_results("SELECT COUNT(*) as num_rows FROM " . $wp_w3bs_table);

    if (!$rows[0]->num_rows) {
        $wpdb->insert($wp_w3bs_table, array(
            'token' => $token,
            'created_at' => $dateTime
        ));
    } else {
        $wpdb->update($wp_w3bs_table, array(
            'token' => $token
        ), array('token_created' => 1));
    }

    $callback_url = admin_url('admin-post.php?action=auth_w3bstore');
    $site_title = get_bloginfo('name');
    $params = http_build_query([
        'auth' => $site_title,
        'token' => $token,
        'callback_url' => $callback_url
    ]);

    wp_redirect(W3B_AUTH_URL . '?' . $params);
}

function w3bs_auth_w3bstore()
{
    global $wpdb, $table_prefix;
    $tblname = 'w3bstore_auth';
    $token = $_GET['token'];
    $objDateTime = new DateTime('NOW');
    $dateTime =  $objDateTime->format('Y-m-d H:i:s');
    $wp_w3bs_table = $table_prefix . "$tblname";
    $wpdb->update($wp_w3bs_table, array(
        'w3bstore_token' => $token,
        'auth_at' => $dateTime,
    ), array('token_created' => 1));
    exit(wp_safe_redirect(W3bStore_Admin::get_relative_dashboard_url()));
}
